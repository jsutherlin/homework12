@ Array
@ R4 is the row index
			row			.req R4
@ R5 is the colum index
			column		.req R5
@ R6 Random Number
			random		.req R6
@Sum
@ R2 Number at (0,0) of Array
			int1		.req R2
@ R4 Number at (0,2) of Array
			int2		.req R4
@ R5 Number at (1,1) of Array
			int3		.req R5
@ R6 Number at (1,3) of Array
			int4		.req R6
			
			
	.global main
	
main:

@display name using printf

	LDR R0, =name
	BL printf

@display the current date and time using the date command

	MOV R0, #0
	BL time
	LDR R1, =epoch
	STR R0, [R1]
	LDR R0, =epoch
	BL ctime
	BL printf
	
@get the day of the year

	LDR R0, =epoch
	BL localtime
	LDR R1, =broken_down
	STR R0, [R1]
	LDR R1, [R0, #28]
	ADD R1, R1, #1
	
@seed the rand function with the day of the year

	MOV R0, R1
	BL srand
	
@create a 2 x 4 array and fill with the random numbers between 0 and 127

@for (R4 = 0, R4++, R4 < row)

	MOV R4, #0
	
outer_for:

@for (R5 = 0, R5++, R5 < column)

	MOV R5, #0	
	
inner_for:

@do

	BL rand
	MOV R1, #128
	BL divide
	MOV R6, R1
	
@Save value in array

	MOV R0, R4
	MOV R1, R5
	MOV R2, R6
	BL set

@Increment column index

	ADD R5, R5, #1
	CMP R5, #col
	
@while (R5 < col)

	BLT inner_for
	
@Increment row index

	ADD R4, R4, #1
	CMP R4, #row
	
@while (R4 < row)
	
	BLT outer_for
	

@dump the values in the array using the dump entry point in array.s

	BL dump

@sum the values in (0,0), (0,2), (1,1), (1,3)

	MOV R0, #0
	MOV R1, #0
	BL get
	MOV R2, R0
	
	MOV R0, #0
	MOV R1, #2
	BL get
	MOV R4, R0
	
	MOV R0, #1
	MOV R1, #1
	BL get
	MOV R5, R0
	
	MOV R0, #1
	MOV R1, #3
	BL get
	MOV R6, R0
	
	ADD R0, R2, R4
	ADD R0, R0, R5
	ADD R0, R0, R6
	
@display the sum using the printf function

	MOV R1, R0
	LDR R0, =sum
	BL printf

_exit:
	
	MOV R7, #1
	SWI 0
	
.data
name:
.asciz "James Sutherlin\n"
sum:
.asciz "The sum of (0,0), (0, 2), (1,1) and (1,3) is: %d\n"
epoch:
.word 0
broken_down:
.word 0
format:
	.asciz "Row: %d  Column: %d  Value: %d\n"
	
	.equ row, 2
	.equ col, 4
